window.fn = {};

window.fn.open = function() {
  var menu = document.getElementById('menu');
  menu.open();
};

window.fn.load = function(page) {
  var content = document.getElementById('content');
  var menu = document.getElementById('menu');
  content.load(page)
    .then(menu.close.bind(menu));
};

ons.disableIconAutoPrefix();

document.addEventListener('init', function(event) {
    var page = event.target;
    if (page.id === 'esurvey-question') {
        $('.slick-carousel').slick({
            infinite: false,
            dots: true,
            nextArrow: '<button class="button--outline btn-nextprev slick-next">NEXT</button>',
            prevArrow: '<button class="button--outline btn-nextprev slick-prev">BACK</button>',
        });
    }

    if (page.id === 'home') {
        $('.splashscreen').delay(2000).fadeOut('slow'); 
    }

    if (page.id === 'qrscanner') {
        let scanner = new Instascan.Scanner({ video: document.getElementById('qrcamera') });
        scanner.addListener('scan', function (content) {
            console.log(content);
        });

        Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
        }).catch(function (e) {
            console.error(e);
        });
    }
});
