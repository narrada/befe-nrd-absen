$(document).ready(function() {

//PARSE JSON
    $.ajax({
        url: 'http://13.251.120.95/api/absen/today?api_key=oswwcwc0sw84s4k4owcock0cskc40k4cckwco8sw',
        // url: 'today.json',
        dataType: 'jsonp',
        success: function(json){
            for (var counter = 0; counter < json.data.length; counter++) {
                if (json.data[counter].job_title == 'sec'){
                    json.data[counter].job_title_new = 'Secretary'; 
                } else if (json.data[counter].job_title == 'hof'){
                    json.data[counter].job_title_new = 'Head of Finance'
                } else if (json.data[counter].job_title == 'hrm'){
                    json.data[counter].job_title_new = 'Human Resource Manager'
                } else if (json.data[counter].job_title == 'hom'){
                    json.data[counter].job_title_new = 'Head of Media'
                } else if (json.data[counter].job_title == 'cw'){
                    json.data[counter].job_title_new = 'Copy Writer'
                } else if (json.data[counter].job_title == 'its'){
                    json.data[counter].job_title_new = 'IT Support'
                } else if (json.data[counter].job_title == 'cgh'){
                    json.data[counter].job_title_new = 'Creative Group Head'; 
                } else if (json.data[counter].job_title == 'fin'){
                    json.data[counter].job_title_new = 'Finance'
                } else if (json.data[counter].job_title == 'afin'){
                    json.data[counter].job_title_new = 'Assistance Finance'
                } else if (json.data[counter].job_title == 'dir'){
                    json.data[counter].job_title_new = 'Dirs'
                } else if (json.data[counter].job_title == 'fed'){
                    json.data[counter].job_title_new = 'Front End Developer'
                } else if (json.data[counter].job_title == 'ad'){
                    json.data[counter].job_title_new = 'Art Director'
                } else if (json.data[counter].job_title == 'sad'){
                    json.data[counter].job_title_new = 'Senior Art Director'
                } else if (json.data[counter].job_title == 'bed'){
                    json.data[counter].job_title_new = 'Back End Developer'
                } else if (json.data[counter].job_title == 'snmo'){
                    json.data[counter].job_title_new = 'Social New Media Officer'
                } else if (json.data[counter].job_title == 'nmo'){
                    json.data[counter].job_title_new = 'New Media Officer'
                } else if (json.data[counter].job_title == 'smo'){
                    json.data[counter].job_title_new = 'Social Media Officer'
                } else if (json.data[counter].job_title == 'sms'){
                    json.data[counter].job_title_new = 'Social Media Strategist'
                } else if (json.data[counter].job_title == 'ssms'){
                    json.data[counter].job_title_new = 'Senior Social Media Strategist'
                } else if (json.data[counter].job_title == 'jsms'){
                    json.data[counter].job_title_new = 'Junior Social Media Strategist'
                } else if (json.data[counter].job_title == 'gd'){
                    json.data[counter].job_title_new = 'Graphic Designer'
                } else if (json.data[counter].job_title == 'ae'){
                    json.data[counter].job_title_new = 'Account Executive'
                } else if (json.data[counter].job_title == 'cd'){
                    json.data[counter].job_title_new = 'Creative Director'
                } else if (json.data[counter].job_title == 'aad'){
                    json.data[counter].job_title_new = 'Associate Account Director'
                } else if (json.data[counter].job_title == 'am'){
                    json.data[counter].job_title_new = 'Account Manager'
                } else if (json.data[counter].job_title == 'jam'){
                    json.data[counter].job_title_new = 'Junior Account Manager'
                } else if (json.data[counter].job_title == 'sae'){
                    json.data[counter].job_title_new = 'Senior Account Executive'
                } else if (json.data[counter].job_title == 'hop'){
                    json.data[counter].job_title_new = 'Head of Programmer'
                } else if (json.data[counter].job_title == 'driver'){
                    json.data[counter].job_title_new = 'Driver'
                } else if (json.data[counter].job_title == 'kurir'){
                    json.data[counter].job_title_new = 'Kurir'
                } else if (json.data[counter].job_title == 'fo'){
                    json.data[counter].job_title_new = 'Front Office'
                } else if (json.data[counter].job_title == 'sp'){
                    json.data[counter].job_title_new = 'Strategic Planner'
                } else if (json.data[counter].job_title == 'it_adm'){
                    json.data[counter].job_title_new = 'IT Admin'
                } else if (json.data[counter].job_title == 'med_adm'){
                    json.data[counter].job_title_new = 'Media Admin'
                } else if (json.data[counter].job_title == 'hrs'){
                    json.data[counter].job_title_new = 'Human Resources'
                }

                //ISOTOPE
                var $checkboxes = $('#filters input');
                    $grid = $('#porto-filter-container').isotope({
                        itemSelector: '.absen-list',
                    });


                var $items = $('<div id="id'+ counter +'" class="col-md-3 col-sm-6 col-xs-12 grid-item absen-list '+ json.data[counter].job_title +'"><div class="panel panel-success"><div class="panel-heading"><div class="media"><div class="media-left"><img src="https://www.w3schools.com/bootstrap/img_avatar1.png" class="media-object" style="width:60px"></div><div class="media-body"><h4 class="media-heading">' + json.data[counter]["staff.nickname"] + '</h4><p class="title">'+ json.data[counter].job_title_new +'</p><p class="in">'+ json.data[counter].in2 +'</p></div></div></div></div></div>');
                    $grid.append($items).isotope('insert',$items);


                // $('#porto-filter-container').append('<div id="id'+ counter +'" class="col-md-3 col-sm-6 col-xs-12 grid-item absen-list '+ json.data[counter].job_title +'"><div class="panel panel-success"><div class="panel-heading"><div class="media"><div class="media-left"><img src="https://www.w3schools.com/bootstrap/img_avatar1.png" class="media-object" style="width:60px"></div><div class="media-body"><h4 class="media-heading">' + json.data[counter]["staff.nickname"] + '</h4><p class="title">'+ json.data[counter].job_title_new +'</p><p class="in">'+ json.data[counter].in2 +'</p></div></div></div></div></div>');
                // if (counter < 8){
                //     $('.grid-item').each(function() {
                //         $(this).removeClass('hidden');
                //     });
                // }
            }

            // var jt_unique = [];
            // for (var counter = 0; counter < json.data.length; counter++) {
            //     var job_title = json.data[counter].job_title;
            //     if(jt_unique.indexOf(job_title)==-1){
            //         jt_unique.push(job_title);
            //         // console.log(jt_unique);
            //         $('#filters').append('<input class="chk-btn" type="checkbox" name="'+ json.data[counter].job_title +'" value=".'+ json.data[counter].job_title +'" id="'+ json.data[counter].job_title +'"><label for="'+ json.data[counter].job_title +'">'+ json.data[counter].job_title_new +'</label>');
            //     }
            // }

            $checkboxes.change(function(){
                var filters = [];
                $checkboxes.filter(':checked').each(function(){
                    filters.push( this.value );
                });
                filters = filters.join(', ');
                $grid.isotope({ stagger: 30,transitionDuration: 0,filter: filters });
            });
            // $('.filter-button-group').on( 'click', 'button', function() {
            //   var filterValue = $(this).attr('data-filter');
            //   $grid.isotope({ filter: filterValue });
            // });

var initShow = 4; 
var count = initShow; 
var iso = $grid.data('isotope'); 

loadMore(initShow); 

function loadMore(toShow) {
    // console.log(toShow);
    $grid.find(".hidden").removeClass("hidden");

    var hiddenElems = iso.filteredItems.slice(toShow, iso.filteredItems.length).map(function(item) {
        return item.element;
    });
    $(hiddenElems).addClass('hidden');
    $grid.isotope('layout');

    if (hiddenElems.length == 0) {
        $("#load-more").hide();
    } else {
        $("#load-more").show();
    };
}

$("#load-more").click(function() {
    count = count + initShow;
    loadMore(count);
});

$checkboxes.change(function(){
    if ($("#filters input:checkbox:checked").length == 0){
        loadMore(initShow);
    }
});

var qsRegex;
var $quicksearch = $('#search').keyup(debounce(function() {
    qsRegex = new RegExp( $quicksearch.val(), 'gi' );
    $grid.isotope({
        stagger: 30,
        transitionDuration: 0,
        filter: function() {
            var $this = $(this);
            var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
            return searchResult;
        }
    });
}));
function debounce( fn, threshold ) {
    var timeout;
    threshold = threshold || 100;
    return function debounced() {
        clearTimeout( timeout );
        var args = arguments;
        var _this = this;
        function delayed() {
        fn.apply( _this, args );
    }
        timeout = setTimeout( delayed, threshold );
    };
}

        }
    });
        

//SEARCH
    // $('#search').keyup(function () {
    //     var yourtext = $(this).val();
    //     if (yourtext.length > 0) {
    //         var abc = $(".grid-item").filter(function () {
    //             var str = $(this).text();
    //             var re = new RegExp(yourtext, "i");
    //             var result = re.test(str);
    //             if (!result) {
    //                 return $(this);
    //             }
    //         }).hide();
    //     } else {
    //         $(".grid-item").show();
    //     }
    // });  

//INIT AOS
    // AOS.init();

});





